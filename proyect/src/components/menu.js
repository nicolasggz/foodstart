import React,{useState} from "react";
import FormIngredientes from "./formIngredientes";
import TablaIngredientes from "./tablaIngredientes";

function Menu(){ 
    const [recetas, setRecetas]=React.useState([])

    React.useEffect(() => {
        obtenerDatos()
    }, [])

    const obtenerDatos = async() =>{
        const data = await fetch('https://random-recipes.p.rapidapi.com/ai-quotes/50',{
            method: 'GET',
            headers: {
              'x-rapidapi-host': 'random-recipes.p.rapidapi.com',
              'x-rapidapi-key': '8ea52e235cmsh73e2c6ccceb92ecp13153fjsnd3ae8a1c7c6c'
            }
        })
        const comidas=await data.json()
        console.log(comidas)
        setRecetas(comidas)  
    }
    //Array por defecto de ingrediente
    const ingredientesData=[
        {id:1, ingredientes:'pepino', cantidad:'2'},
        {id:2, ingredientes:'morron', cantidad:'5'},
        {id:3, ingredientes:'cebolla', cantidad:'1'}
    ]
    //Agrega ingrediente
    const addIngrediente= (ingrediente) =>{
        ingrediente.id=ingredientes.length+1 
        setIngredientes([
            ...ingredientes,
            ingrediente            
        ]) 
    }
    //state
    const [ingredientes,setIngredientes]=useState(ingredientesData);

    //Eliminar ingrediente
    const borrarIngrediente= (id) =>{
        setIngredientes(ingredientes.filter(ingrediente => ingrediente.id !== id))
    }

    return(
        <div className="todo_menu">
            <div className="form-view-ingredientes">
                <h3>Ingresar ingredientes</h3>
                <FormIngredientes addIngrediente={addIngrediente}/>
            </div>
            <div>
                <h4>Ver ingredientes</h4>
                <TablaIngredientes ingredientes={ingredientes} borrarIngrediente={borrarIngrediente}/>
            </div>
            <div>
                <h4>Ver recetas</h4>
                <div className="todo_tablero">
                    <table>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Receta</th>
                                <th>Imagen</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {   //muestra de forma dinamica las diferentes recetas
                                recetas.map(comidas =>(
                                    <tr key={comidas.id}>
                                        <td>{comidas.id}</td>
                                        <td>{comidas.title}</td>
                                        <td><img src={comidas.image} height="100px" width="100px"></img></td>
                                        <td><button className="boton-ver-mas">Ver Mas</button></td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}
export default Menu;