import React from "react";

const formIngredientes=(props)=>{
    return(
        <table>
            <thead>
                <tr>
                    <th>Ingrediente</th>
                    <th>Cantidad</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                {   //muestra de forma dinamica los ingredientes y cantidades
                    props.ingredientes.length > 0?
                    props.ingredientes.map(ingrediente =>(
                        <tr key={ingrediente.id}>
                            <td>{ingrediente.ingredientes}</td>
                            <td>{ingrediente.cantidad}</td>
                            <td>
                                <button className="boton-editar">Editar</button>
                                <button className="boton-borrar" onClick={()=>{props.borrarIngrediente(ingrediente.id)}}>Borrar</button>
                            </td>
                        </tr>
                    )) :(
                        <tr>
                            <td colSpan={3}>Sin ingredientes</td>
                        </tr>
                    )
                }
            </tbody>
        </table>
    )
}


export default formIngredientes;

