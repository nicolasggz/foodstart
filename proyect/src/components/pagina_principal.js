import React from "react";
import {Link} from "react-router-dom";

function PaginaPrincipal(){
    return(//Toda la pagina principal se muestra 
        <div className="pagina">
            <header>
                    <nav>
                        <Link to="/pagina_principal" className="btn">  
                            Inicio
                        </Link>
                        <Link to="/login" className="btn">
                            Iniciar Sesion
                        </Link>
                        <Link to="/menu" className="btn" activeClassName="active">
                            Menu
                        </Link>
                    </nav>
                <title>FoodStart</title>
                <section className="textos-header">
                    <h1 className="titulo">Podras crear tus comidas</h1>
                    <h2 className="titulo">-FOODSTART-</h2>
                </section>
            </header>
            <main>
            </main>
        </div>
    );
}
export default PaginaPrincipal;