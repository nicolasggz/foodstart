import React from "react";
import { useForm } from "react-hook-form";

const FormIngredientes=(props)=>{

    const {register, handleSubmit} = useForm();

    const onSubmit=(data,e)=>{
        console.log(data)

        props.addIngrediente(data)

        e.target.reset();
    }

    return(
        <div className="todo-ingredientes">
            <form onSubmit={handleSubmit(onSubmit)}> 
                <label>Ingredientes</label>
                <input type="text" className="input-ingrediente" {...register('ingredientes',{required:true})}/>
                <label>Cantidad</label>
                <input type="text" className="input-cantidad" {...register('cantidad',{required:true})}/>
                <input type="submit" value="Agregar" />
            </form>
        </div>
    )

}


export default FormIngredientes;