import React from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './assets/App.css';
import './assets/login.css';
import './assets/bootstrap.min.css';
import Login from './components/login';
import PaginaPrincipal from './components/pagina_principal';
import Menu from './components/menu';

function App(){
    return(
        <Router>
            <Switch>
                <Route exact path="/" component={PaginaPrincipal}/>
                <Route path="/pagina_principal" component={PaginaPrincipal}/>
                <Route path="/login" component={Login}/>
                <Route path="/menu" component={Menu}/>
            </Switch>
        </Router>
    );
}
export default App;
